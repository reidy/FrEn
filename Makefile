CC       = gcc
LIBS     = -lm
CFLAGS   = -Wall -Wextra -ansi -pedantic -g
LDFLAGS  = -fsanitize=address -fsanitize=undefined -fsanitize=shift -fsanitize=integer-divide-by-zero -fsanitize=unreachable -fsanitize=vla-bound -fsanitize=null -fsanitize=return -fsanitize=signed-integer-overflow -fsanitize=bool -fsanitize=enum
RM       = rm -rf

HEADDIR  = ./src
SRCDIR   = ./src
OBJDIR   = ./obj
BINDIR   = ./

SOURCES  = $(wildcard $(SRCDIR)/*.c)
INCLUDES = $(wildcard $(HEADDIR)/*.h)
OBJECTS  = $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)
BINARY = $(BINDIR)/main

all: $(BINARY)

$(BINARY): $(OBJECTS)
	@$(CC) $(LIBS) $(LDFLAGS) $^ -o $@
	@echo "Compiled" $@ "successfully."

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@mkdir -p $(OBJDIR)
	@$(CC) $(CFLAGS) -c $< -o $@
	@echo "(CC) "$<""

.PHONY: clean
clean:
	@$(RM) $(OBJECTS)
	@echo "Objects removed."

.PHONY: remove
remove: clean
	@$(RM) $(BINDIR)/$(BINARY)
	@echo "Executable removed."
