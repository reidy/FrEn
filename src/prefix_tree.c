/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "prefix_tree.h"

prefix_tree *pt_alloc (void)
{
    prefix_tree *pt = malloc (sizeof (prefix_tree));
    pt->size = 0;
    pt->t_word = NULL;

    pt->current = malloc (sizeof (tree));
    pt->current->left_son = NULL;
    pt->current->right_son = NULL;

    pt->root = pt->current;

    return pt;
}

void pt_add_word (prefix_tree *pt, char *word, unsigned int code)
{
    int i, s_word;

    s_word = strlen ((const char *)word);

    for (i = 0; i < s_word; i++)
    {
        pt_search_and_go (pt, word[i]);
        pt->current->n->letter = word[i];
    }
    pt->current->n->code = code;

    pt_reset_root (pt);
}

void pt_search_and_go (prefix_tree *pt, char letter)
{
    tree *t_left, *t_right;
    t_left = pt->current->left_son;
    t_right = pt->current->right_son;

    if (t_left != NULL)
    {
        if (t_left->n->letter == letter)
        {
            pt->current = t_left;
        }
        else
        {
            if (t_right == NULL)
            {
                pt->current->right_son = malloc (sizeof (tree));
                pt->current->right_son->left_son = NULL;
                pt->current->right_son->right_son = NULL;
            }

            pt->current = pt->current->right_son;
            pt_search_and_go (pt, letter);
        }
    }
    else
    {
        pt->current->left_son = calloc (sizeof (tree), 1);
        pt->current->left_son->n = calloc (sizeof (node), 1);

        pt->current = pt->current->left_son;
    }
}

int pt_search (prefix_tree *pt, char letter)
{
    int code;
    tree *before_rec;
    tree *t_left, *t_right;
    t_left = pt->current->left_son;
    t_right = pt->current->right_son;

    if (t_left != NULL)
    {
        if (t_left->n->letter == letter)
        {
            pt->current = t_left;
            return pt->current->n->code;
        }
        else
        {
            if (t_right == NULL)
            {
                return UNDEFINED_WORD;
            }

            before_rec = pt->current;
            pt->current = pt->current->right_son;
            if ((code = pt_search (pt, letter)) == UNDEFINED_WORD)
            {
                pt->current = before_rec;
                return code;
            }
            else
            {
                return code;
            }
        }
    }
    else
    {
        return UNDEFINED_WORD;
    }
}

/* Return to the root */
void pt_reset_root (prefix_tree *pt)
{
    pt->current = pt->root;
}

/* Display the tree */
void pt_print (tree *t)
{
    tree *t_left, *t_right;
    t_left = t->left_son;
    t_right = t->right_son;

    if (t_left != NULL)
    {
        printf ("L (%c, %d)\n", t_left->n->letter, t_left->n->code);
        pt_print (t_left);
    }
    if (t_right != NULL)
    {
        printf ("R\n");
        pt_print (t_right);
    }
}

void pt_print_tab_word (prefix_tree *pt)
{
    unsigned int i;

    printf ("Reverse tree:\n");

    for (i = 1; i <= pt->size; i++)
    {
        printf ("ID %u: %s\n", i, pt->t_word[i]);
    }
}

/* Check if the tree is empty */
void pt_empty (tree *t)
{
    if (t->left_son != NULL)
    {
        if (t->left_son->n != NULL)
        {
            free (t->left_son->n);
        }
        pt_empty (t->left_son);
    }

    if (t->right_son != NULL)
    {
        pt_empty (t->right_son);
    }
    free (t);
}

/* Free memory */
void pt_free (prefix_tree *pt)
{
    unsigned int i;

    pt_empty (pt->root);

    for (i = 1; i <= pt->size; i++)
    {
        free (pt->t_word[i]);
    }
    free (pt->t_word);

    free (pt);
}
