/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BIGRAM__H
#define BIGRAM__H

#include "conf.h"

void bigram_alloc (unigram *u);
void bigram_print (bigram *bi);
void bigram_add (bigram *bi, int code);
void bigram_add_first (bigram *bi, int code);
void bigram_incr (bigram *bi, int code);
void bigram_free (bigram *bi);

#endif
