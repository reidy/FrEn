/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PARSER__H
#define PARSER__H

#include "conf.h"

void parser_read_lexicon (prefix_tree *pt, io *lexicon, debug *d);
void parser_read_corpus (prefix_tree *pt, io *corpus, hashgram *h,
                         debug *d, int lang);
void parser_read_perplex (perplexity *p, debug *d);
void parser_read_translation_table (hashtrans *tt, debug *d);
void parser_read_lattice (prefix_tree *pt, lattice *lat, hashtrans *tt,
                          perplexity *pplex, debug *d, int lang);
void parser_write_sequence (unsigned int *seq, lattice *lat,
                            language *en, debug *d);

#endif
