/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UNIGRAM__H
#define UNIGRAM__H

#include "conf.h"

void unigram_alloc (hashgram *h);
void unigram_init (hashgram *h);
void unigram_print (hashgram *h);
void unigram_incr (hashgram *h, int code);
float unigram_search_proba (unigram u, int code);
void unigram_free (unigram *u);


#endif
