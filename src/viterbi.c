/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "viterbi.h"

/* Compute viterbi */
unsigned int *viterbi (lattice *lat, hashtrans *tt, hashgram *en,
                       debug *d)
{
    unsigned int *seq = malloc (sizeof (unsigned int) * (lat->size));
    unsigned int i, j, k, new_k;
    float tmp_alpha;
    float logprob = 0.0;
    float tmp_min_alpha = INT_MAX;
    float tmp_new_min_alpha = INT_MAX;
    translation *t_trans;

    /* Init the first value of the lattice */
    for (j = 0; j < lat->t_word[0]->size; j++)
    {
        t_trans = lat->t_word[0]->t_trans[j];
        t_trans->alpha =
            hashtrans_find_logprob (tt, lat->t_word[0]->id, t_trans->id)
            + en->u[t_trans->id].p; 

        t_trans->beta = 0;

        if (tmp_min_alpha >= t_trans->alpha)
        {
            tmp_min_alpha = t_trans->alpha;
            k = j;
        }
    }
    seq[0] = lat->t_word[0]->t_trans[k]->id;

    if (lat->size <= 1)
    {
        return seq;
    }

    /* Recursion with i - 1 */
    for (i = 1; i < lat->size; i++)
    {
        if (d->b_viterbi)
        {
            printf ("Viterbi %d/%d\n", i, lat->size);
        }
        for (j = 0; j < lat->t_word[i]->size; j++)
        {
            t_trans = lat->t_word[i]->t_trans[j];
            logprob = hashtrans_find_logprob (tt, lat->t_word[i]->id,
                                              t_trans->id)
                      + en->u[t_trans->id].p;

            tmp_alpha = tmp_min_alpha + logprob;

            if (t_trans->alpha >= tmp_alpha)
            {
                t_trans->alpha = tmp_alpha;
                t_trans->beta = lat->t_word[i - 1]->t_trans[k]->id;
            }
            if (tmp_new_min_alpha >= t_trans->alpha)
            {
                tmp_new_min_alpha = t_trans->alpha;
                seq[i - 1] = t_trans->beta;
                new_k = j;
            }
        }
        if (j != lat->size - 1)
        {
            k = new_k;
        }
        tmp_min_alpha = tmp_new_min_alpha;
        tmp_new_min_alpha = INT_MAX;
    }
    seq[i - 1] = lat->t_word[i - 1]->t_trans[k]->id;

    return seq;
}
