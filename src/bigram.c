/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "unigram.h"

void bigram_alloc (unigram *u)
{
    u->bi = malloc (sizeof (bigram));
    u->bi->p = 0.0;
    u->bi->next = NULL;
}

void bigram_print (bigram *bi)
{
    if (bi != NULL)
    {
        printf ("\t word : %d | counter : %d | proba : %f\n",
                bi->id, bi->c, bi->p);
        bigram_print (bi->next);
    }
}

void bigram_add (bigram *bi, int code)
{
    bi->next = malloc (sizeof (bigram));
    bi->next->next = NULL;
    bi->next->id = code;
    bi->next->c = 1;
    bi->next->p = 0.0;
}

void bigram_add_first (bigram *bi, int code)
{
    bi->c = 1;
    bi->p = 0.0;
    bi->id = code;
    bi->next = NULL;
}

void bigram_incr (bigram *bi, int code)
{
    if (bi->id == code)
    {
        bi->c++;
    }
    else if (bi->id == UNDEFINED_WORD)
    {
        bigram_add_first (bi, code);
    }
    else if (bi->next != NULL)
    {
        bigram_incr (bi->next, code);
    }
    else
    {
        bigram_add (bi, code);
    }
}

void bigram_free (bigram *bi)
{
    if (bi != NULL)
    {
        if (bi->next != NULL)
        {
            bigram_free (bi->next);
        }
        free (bi);
    }
}
