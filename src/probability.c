/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "probability.h"

/* Compute the log */
double probability_log (double x, double base)
{
	return ((log (base)) != 0) ? (log (x) / log (base)) : 0;
}

/* Compute the unigram proba */
void probability_unigram (hashgram *h)
{
	unsigned int i;
	
	for (i = 0; i < h->size; i++)
	{
		h->u[i].p = -probability_log (((double) h->u[i].c / h->n_words),
                                      ((double) 2));

		if (h->u[i].bi != NULL)
		{
			probability_bigram (h->u[i].c, h->u[i].bi);
		}
	}
}

/* Compute the bigram proba */
void probability_bigram (unsigned int c, bigram *bi)
{
	if (bi->next == NULL)
	{
		bi->p = - probability_log (((double) bi->c / c), ((double) 2));
	}
	else
	{
		bi->p = - probability_log (((double) bi->c / c), ((double) 2));
		probability_bigram (c, bi->next);
	} 
}

/* Launch the probability computation */
void probability_launch (hashgram *h)
{
    hashgram_n_words (h);
    probability_unigram (h);
}
