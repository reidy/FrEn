/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "equivalence.h"

equivalence **equivalence_alloc (unsigned int size)
{
    unsigned int i;

    equivalence **t_equiv = malloc (sizeof (equivalence*) * size);

    for (i = 0; i < size; i++)
    {
        t_equiv[i] = calloc (sizeof (equivalence), 1);
    }

    return t_equiv;
}

void equivalence_init (equivalence *t_equiv, unsigned int size)
{
    t_equiv->size = size;
    t_equiv->t_trans = translation_table_alloc (size);
}

void equivalence_free (equivalence *t_equiv)
{
    unsigned int i;

    if (t_equiv->t_trans != NULL)
    {
        for (i = 0; i < t_equiv->size; i++)
        {
            translation_table_free (t_equiv->t_trans[i]);
        }
        free (t_equiv->t_trans);
        free (t_equiv);
    }
}
