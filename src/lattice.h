/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LATTICE__H
#define LATTICE__H

#include "conf.h"

lattice *lattice_alloc (void);
void lattice_init (lattice *lat, unsigned int size, debug *d);
void lattice_init_io (lattice *lat);
void lattice_add_word (lattice *lat, hashtrans *table, int i, int c_fr,
                       bool new_line);
void lattice_print (lattice *lat);
void lattice_free (lattice *l);

#endif
