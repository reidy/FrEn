/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONF__H
#define CONF__H

#include <unistd.h>
#include <getopt.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "prefix_tree.h"

#define U __attribute__((unused))
#define MAX_WORD_LENGTH 50

#define DEFAULT_EN_LEXICON "./docs/files/lextable.en.txt"
#define DEFAULT_EN_CORPUS "./docs/files/train.en.txt"
#define DEFAULT_EN_PERPLEXITY "./docs/files/perplexity.en.txt"

#define DEFAULT_FR_LEXICON "./docs/files/lextable.fr.txt"
#define DEFAULT_FR_CORPUS "./docs/files/train.fr.txt"
#define DEFAULT_FR_PERPLEXITY "./docs/files/perplexity.fr.txt"

#define DEFAULT_TRANSLATION_TABLE "./docs/files/translate_table_2.en_fr"
#define DEFAULT_LATTICE_SEQ "./docs/files/sequence_to_translate.txt"

#define TRANSLATION_TABLE_MAX_LINE_SIZE 25

#define FR 0
#define EN 1

#ifndef isnan
#define isnan(x) ((x) != (x))
#endif

/* What a boolean is */
typedef enum { FALSE, TRUE } bool;


/* To manage input and output file. */
typedef struct
{
    FILE *file;
    char *location;
    unsigned int mode;
    bool custom;
} io;

#include "io.h"


/* Using in cmdline to print and debug algorithms. */
typedef struct
{
    bool b_prefix_tree; /* to display the prefix tree */
    bool b_lexicon;     /* to display the parsing of lexicon */
    bool b_corpus;      /* to display the parsong of corpus */
    bool b_hashgram;    /* to display the hashgram */
    bool b_perplexity;  /* to display perplexity */
    bool b_translation; /* to display perplexity */
    bool b_viterbi;     /* to display viterbi */
    bool b_perplex_analysis; /* to display perplexity analysis */
} debug;

#include "debug.h"


/* Hashgram table registering the unigram and bigram probabilities. */
typedef struct bigram bigram;
struct bigram
{
    int id;          /* id of the word */
    unsigned int c;  /* occurences of this word */
    float p;         /* probability of the bigram (-logprob) */
    bigram *next;
};

typedef struct
{
    unsigned int c; /* number of occurences of this word (unigram) */
    float p;       	/* probability of the unigram (-logprob) */
    bigram *bi;     /* list of associated words (bigram) */
} unigram;

typedef struct
{
    unigram *u;           /* every unigram (perfect hash) */
    unsigned int n_words; /* total number of words */
    unsigned int size;    /* size of the lexique */
} hashgram;

#include "hashgram.h"
#include "unigram.h"
#include "bigram.h"


/* Used in perplexity computation. */
typedef struct 
{
	unsigned int *seq;		/* the sequence itself */
	unsigned int size;	/* the size of the sequence of words */
	float pplex;		/* the perplexity computed */
    bool custom;        /* Custom sequence */
    io *pplx_file;
} perplexity;

#include "probability.h"
#include "perplexity.h"


/* Translate table hashmap structure */
typedef struct
{
    unsigned int c_en;
    float logprob;
} translation_table;

typedef struct
{
    unsigned int size;
    translation_table **t_trans;
} equivalence;

typedef struct 
{
    unsigned int size;
    equivalence **t_equiv;
    io *tt_file;
} hashtrans;

#include "translation_table.h"
#include "equivalence.h"
#include "hashtrans.h"


/* Lattice hashmap structure */
typedef struct
{
    int id;
    float alpha;
    unsigned int beta;
} translation;

typedef struct
{
    int id;
    unsigned int size;
    translation **t_trans;
    bool new_line;
} word;

typedef struct
{
    unsigned int size;
    word **t_word;
    io *lat_file;
} lattice;

#include "translation.h"
#include "word.h"
#include "lattice.h"


/* Manage a set of needed structs to load a language. */
typedef struct
{
    hashgram *hash;
    io *lexi;
    io *corp;
    prefix_tree *pt;
    perplexity *perplex;
    unsigned int type;
} language;

#include "language.h"


/* Manage a set of needed structs to load a translation process. */
typedef struct
{
    hashtrans *table;
    lattice *lat;
    perplexity *pplex;
} translate;

#include "translate.h"

/* General includes. */
#include "parser.h"
#include "cmdline.h"

#endif
