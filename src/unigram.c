/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "unigram.h"

void unigram_alloc (hashgram *h)
{
    h->u = malloc (sizeof (unigram) * h->size);
}

void unigram_init (hashgram *h)
{
    unsigned int i;

    for (i = 0; i < h->size; i++)
    {
        h->u[i].c = 0;
        h->u[i].p = 0.0;
        h->u[i].bi = NULL;
    }
}

void unigram_print (hashgram *h)
{
    unsigned int i;

    for (i = 1; i < h->size; i++)
    {
        if (h->u[i].c != 0)
        {
            printf ("word : %d | counter : %d | proba : %f\n", 
                            i,             h->u[i].c,   h->u[i].p);
            bigram_print (h->u[i].bi);
        }
    }
}

float unigram_search_proba (unigram u, int code)
{
    bigram *tmp;

    for (tmp = u.bi; tmp != NULL; tmp = tmp->next)
    {
        if (tmp->id == code)
        {
            return tmp->p;
        }
    }
    return u.p + 5;
}

void unigram_incr (hashgram *h, int code)
{
    if (h->u[code].bi == NULL)
    {
        bigram_alloc (&h->u[code]);
        h->u[code].bi->id = UNDEFINED_WORD;
        h->u[code].bi->c = 1;
        h->u[code].bi->p = 0.0;
    }
    h->u[code].c++;
}

void unigram_free (unigram *u)
{
    bigram_free (u->bi);
}
