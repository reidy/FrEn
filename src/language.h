/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LANGUAGE__H
#define LANGUAGE__H

#include "conf.h"

language *language_alloc (void);
void language_init (language *l, unsigned int type);
void language_init_data (language *l, debug *d);
void language_init_io (language *l);
void language_parse (language *l, debug *d);
void language_perplex (language *l, debug *d);
void language_free (language *l);

#endif
