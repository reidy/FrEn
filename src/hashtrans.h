/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HASh_TRANS__H
#define HASH_TRANS__H

#include "conf.h"

hashtrans *hashtrans_alloc (void);
void hashtrans_init_io (hashtrans *tt, debug *d);
void hashtrans_init (hashtrans *tt, unsigned int size);
void hashtrans_init_equiv_tables (hashtrans *tt);
float hashtrans_find_logprob (hashtrans *tt, unsigned int c_fr, 
                                             unsigned int c_en);
void hashtrans_free (hashtrans *tt);

#endif
