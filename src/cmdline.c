/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cmdline.h"

void print_help (char *bin_name)
{
        printf ("%s [options]\n", bin_name);
        printf ("Options:\n");
        printf ("  -D, --debug             : "
                    "Debug all.\n");
        printf ("  -L, --debug-lex         : "
                    "Debug lexicons.\n");
        printf ("  -C, --debug-corp        : "
                    "Debug corpuses.\n");
        printf ("  -T, --debug-tree        : "
                    "Debug prefix trees.\n");
        printf ("  -H, --debug-hashgram    : "
                    "Debug hashgrams.\n");
        printf ("  -P, --debug-perplexity  : "
                    "Debug perplexities.\n");
        printf ("  -A, --debug-translation : "
                    "Debug translation.\n");
        printf ("  -V, --debug-viterbi     : "
                    "Debug viterbi algorithm.\n");
        printf ("  -k, --en-lexicon [arg]  : "
                    "English lexicon text file path.\n");
        printf ("  -l, --fr-lexicon [arg]  : "
                    "French lexicon text file path.\n");
        printf ("  -b, --en-corpus [arg]   : "
                    "English corpus text file path.\n");
        printf ("  -c, --fr-corpus [arg]   : "
                    "French corpus text file path.\n");
        printf ("  -o, --en-perplex [arg]  : "
                    "English perplexity text file path.\n");
        printf ("  -p, --fr-perplex [arg]  : "
                    "French perplexity text file path.\n");
        printf ("  -a, --translation [arg] : "
                    "Translation table file path.\n");
        printf ("  -z, --lattice [arg]     : "
                    "File path to translate.\n");
        printf ("  -Y, --perplexity        : "
                    "Enable the perplexity analysis.\n");
        printf ("  -h, --help              : "
                    "Print this help.\n");
}

bool cmdline_read_arg (int argc, char **argv,
                       language *en, language *fr, translate *t,
                       debug *d)
{
    int c, option_index;
    static struct option long_options[] =
    {
        {"debug",             no_argument,       0, 'D'},
        {"debug-lex",         no_argument,       0, 'L'},
        {"debug-corp",        no_argument,       0, 'C'},
        {"debug-tree",        no_argument,       0, 'T'},
        {"debug-hashgram",    no_argument,       0, 'H'},
        {"debug-perplexity",  no_argument,       0, 'P'},
        {"debug-translation", no_argument,       0, 'A'},
        {"debug-viterbi",     no_argument,       0, 'V'},
        {"en-lexicon",        required_argument, 0, 'k'},
        {"fr-lexicon",        required_argument, 0, 'l'},
        {"en-corpus",         required_argument, 0, 'b'},
        {"fr-corpus",         required_argument, 0, 'c'},
        {"en-perplex",        required_argument, 0, 'o'},
        {"fr-perplex",        required_argument, 0, 'p'},
        {"translation",       required_argument, 0, 'a'},
        {"lattice",           required_argument, 0, 'z'},
        {"perplexity",        required_argument, 0, 'Y'},
        {"help",              no_argument,       0, 'h'},
        {0, 0, 0, 0}
    };

    while ((c = getopt_long (argc, argv, "b:c:k:l:o:p:LCDTHPAVa:z:Y:h",
                             long_options, &option_index)) != -1)
    {
        switch (c)
        {
            case 'C':
                d->b_corpus = TRUE;
                break;
            case 'L':
                d->b_lexicon = TRUE;
                break;
            case 'D':
                d->b_lexicon = TRUE;
                d->b_corpus = TRUE;
                d->b_prefix_tree = TRUE;
                d->b_hashgram = TRUE;
                d->b_perplexity = TRUE;
                d->b_translation = TRUE;
                d->b_viterbi = TRUE;
                d->b_perplex_analysis = TRUE;
                break;
            case 'T':
                d->b_prefix_tree = TRUE;
                break;
            case 'H':
                d->b_hashgram = TRUE;
                break;
            case 'o':
                en->perplex->custom = TRUE;
                d->b_perplex_analysis = TRUE;
                break;
            case 'p':
                fr->perplex->custom = TRUE;
                d->b_perplex_analysis = TRUE;
                break;
            case 'P':
                d->b_perplexity = TRUE;
                d->b_perplex_analysis = TRUE;
                break;
            case 'A':
                d->b_translation = TRUE;
                break;
            case 'V':
                d->b_viterbi = TRUE;
                break;
            case 'k':
                en->lexi->custom = TRUE;
                en->lexi->location = optarg;
                break;
            case 'l':
                fr->lexi->custom = TRUE;
                fr->lexi->location = optarg;
                break;
            case 'b':
                en->corp->custom = TRUE;
                en->corp->location = optarg;
                break;
            case 'c':
                fr->corp->custom = TRUE;
                fr->corp->location = optarg;
                break;
            case 'a':
                t->table->tt_file->custom = TRUE;
                t->table->tt_file->location = optarg;
                break;
            case 'z':
                t->lat->lat_file->custom = TRUE;
                t->lat->lat_file->location = optarg;
                break;
            case 'Y':
                d->b_perplex_analysis = TRUE;
                break;
            case 'h':
            case '?':
            case ':':
            default:
                print_help (argv[0]);
                return 1;

            option_index = 0;
        }
    }
    return 0;
}
