/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hashtrans.h"

hashtrans *hashtrans_alloc (void)
{
    hashtrans *tt = malloc (sizeof (hashtrans));

    tt->size = 0;
    tt->t_equiv = NULL;
    tt->tt_file = io_alloc (READ);

    return tt;
}

void hashtrans_init_io (hashtrans *tt, debug *d)
{
    if (!tt->tt_file->custom)
    {
        tt->tt_file->location = DEFAULT_TRANSLATION_TABLE;
    }

    io_open (tt->tt_file);

    if (d->b_translation)
    {
        printf ("%s translation file opened\n", tt->tt_file->location);
    }
}

/* Init with the number of source words in the translate table. */
void hashtrans_init (hashtrans *tt, unsigned int size)
{
    tt->size = size;
    tt->t_equiv = equivalence_alloc (size);
}

void hashtrans_init_equiv_tables (hashtrans *tt)
{
    unsigned int i;

    for (i = 0; i < tt->size; i++)
    {
        equivalence_init (tt->t_equiv[i], tt->t_equiv[i]->size);
    }
}

float hashtrans_find_logprob (hashtrans *tt, unsigned int c_fr, 
                                             unsigned int c_en)
{
    unsigned int i;

    for (i = 0; i < tt->t_equiv[c_fr]->size; i++)
    {
        if (tt->t_equiv[c_fr]->t_trans[i]->c_en == c_en)
        {
            return tt->t_equiv[c_fr]->t_trans[i]->logprob;
        }
    }

    perror ("No translation find for this word.");
    return -1.0;
}

equivalence *hashtrans_fr_word_table (hashtrans *tt, unsigned int c_fr)
{
    return tt->t_equiv[c_fr];
}

void hashtrans_free (hashtrans *tt)
{
    unsigned int i;

    io_free (tt->tt_file);

    if (tt->t_equiv != NULL)
    {
        for (i = 0; i < tt->size; i++)
        {
            equivalence_free (tt->t_equiv[i]);
        }
        free (tt->t_equiv);
    }

    free (tt);
}
