/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lattice.h"

lattice *lattice_alloc (void)
{
    lattice *l = malloc (sizeof (lattice));
    l->size = 0;
    l->t_word = NULL;
    l->lat_file = io_alloc (READ);

    return l;
}

void lattice_init (lattice *lat, unsigned int size, debug *d)
{
    lat->size = size;
    lat->t_word = word_alloc (lat->size);

    if (d->b_translation)
    {
        printf ("Lattice size: %u\n", lat->size);
    }
}

void lattice_init_io (lattice *lat)
{
    if (!lat->lat_file->custom)
    {
        lat->lat_file->location = DEFAULT_LATTICE_SEQ;
    }
    io_open (lat->lat_file);
}

void lattice_add_word (lattice *lat, hashtrans *table, int i, int c_fr,
                       bool new_line)
{
    unsigned int size, j;
    translation_table **to_en;
    translation *t_trans;

    to_en = table->t_equiv[c_fr]->t_trans;
    size = table->t_equiv[c_fr]->size;

    lat->t_word[i] = calloc (sizeof (word), 1);

    word_init (lat->t_word[i], size, c_fr, new_line);

    for (j = 0; j < size; j++)
    {
        lat->t_word[i]->t_trans[j] = malloc (sizeof (translation));
        t_trans = lat->t_word[i]->t_trans[j];
        translation_init (t_trans, to_en[j]->c_en, INT_MAX, 0);
    }
}

void lattice_print (lattice *lat)
{
    unsigned int i, j;
    translation *t;

    printf ("Lattice:\n");
    for (i = 0; i < lat->size; i++)
    {
        printf ("Word %u:\n", lat->t_word[i]->id);
        for (j = 0; j < lat->t_word[i]->size; j++)
        {
            t = lat->t_word[i]->t_trans[j];
            printf ("-> %u\t%f\t%d\n", t->id, t->alpha, t->beta);
        }
        printf ("\n");
    }
}

void lattice_free (lattice *lat)
{
    unsigned int i;

    for (i = 0; i < lat->size; i++)
    {
        word_free (lat->t_word[i]);
        free (lat->t_word[i]);
    }
    free (lat->t_word);

    io_free (lat->lat_file);

    free (lat);
}
