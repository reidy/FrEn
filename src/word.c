/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "word.h"

word **word_alloc (unsigned int size)
{
    word **w = calloc (sizeof (word*), size);

    return w;
}

void word_init (word *w, unsigned int size, unsigned int id,
                bool new_line)
{
    w->size = size;
    w->id = id;
    w->t_trans = translation_alloc (w->size);
    w->new_line = new_line;
}

void word_free (word *w)
{
    unsigned int i;
    
    for (i = 0; i < w->size; i++)
    {
        translation_free (w->t_trans[i]);
    }
    free (w->t_trans);
}
