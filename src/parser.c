/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "parser.h"

/* read the lexicon line by line */
void parser_read_lexicon (prefix_tree *pt, io *lexicon, debug *d)
{
    unsigned int code, nb_word = 1;
    char *word = malloc (sizeof (char) * MAX_WORD_LENGTH);

    while (fscanf (lexicon->file, "%s %u", word, &code) != EOF)
    {
        if (d->b_lexicon)
        {
            printf ("%s %u\n", word, code);
        }
        pt_add_word (pt, word, code); /* Add the word into the tree */
        pt->size = code;              /* Current size of the lexicon */
        nb_word++;
    }

    rewind (lexicon->file);
    pt->t_word = calloc (sizeof (char *), nb_word);

    while (fscanf (lexicon->file, "%s %u", word, &code) != EOF)
    {
        pt->t_word[code] = malloc (sizeof (char) * MAX_WORD_LENGTH);
        strcpy (pt->t_word[code], word);
    }

    if (d->b_prefix_tree)
    {
        pt_print_tab_word (pt);
        pt_print (pt->current);
    }
    free (word);
}

#define is_hard_sep_general(c)(((c) == '(') || ((c) == ')') || \
            ((c) == '-') || ((c) == '\n') || ((c) == ' '))
#define is_hard_sep_en(c)(((c) == ',') || ((c) == ';') || \
            ((c) == ':') || ((c) == '!') || ((c) == '?') || \
            ((c) == '.') || ((c) == '"'))

/* Read the corpus word by word */
void parser_read_corpus (prefix_tree *pt, io *corpus,
                         hashgram *h, debug *d, int lang)
{
    bool b_word_recognized = FALSE;
    bool b_reset = FALSE;

    int i = 0;
    char *word = malloc (sizeof (char) * MAX_WORD_LENGTH);

    int correct_code = UNDEFINED_WORD;
    int read_code = UNDEFINED_WORD;
    int last_code = UNDEFINED_WORD;

    while ((word[i] = fgetc (corpus->file)) != EOF)
    {
        word[i+1] = '\0';

        /* Set special word and code for the new line */
        if (word[0] == '\n')
        {
            strcpy (word, "</s>");
            correct_code = NEW_LINE;
            b_word_recognized = TRUE;
            b_reset = TRUE;
        }
        /* Jump the space character */
        else if (word[0] == ' ')
        {
            b_reset = TRUE;
        }
        /* If the single char is a hard separator */
        else if (is_hard_sep_general (word[0]) ||
                 (lang == EN && is_hard_sep_en (word[0])))
        {
            correct_code = pt_search (pt, word[i]);
            b_word_recognized = TRUE;
            b_reset = TRUE;
        }
        /* If the next char make the word undefined */
        else if ((read_code = pt_search (pt, word[i]))
                                                      == UNDEFINED_WORD)
        {
            /* The liaison char can be written differently */
            if (word[i] == '-')
            {
                if ((read_code = pt_search (pt, '_')) != UNDEFINED_WORD)
                {
                    correct_code = read_code;
                    i++;
                }
            }
            /* If the char is a hard separator, we remove the last
             * letter on the buffer and we keep the last correct_code */
            else if (((is_hard_sep_general (word[i])) ||
                            (lang == EN && is_hard_sep_en (word[i])))
                        || (word[i] == '\''))
            {
                ungetc (word[i], corpus->file);
                word[i--] = '\0';
                b_word_recognized = TRUE;
                b_reset = TRUE;
            }
            /* Or we update the correct_code with the last read_code */
            else
            {
                correct_code = read_code;
                i++;
            }
        }
        /* Stop the research if the last read char is an apostrophe */
        else if (word[i] == '\'' && i != 0)
        {
            correct_code = read_code;
            b_word_recognized = TRUE;
            b_reset = TRUE;
        }
        else
        {
            correct_code = read_code;
            i++;
        }

        if (b_word_recognized)
        {
            /* Print if debug */
            if (d->b_corpus)
            {
                printf ("%s %d\n", word, correct_code);
            }

            if (correct_code > 0)
            {
                /* Increment the counter on the given unigram */
                unigram_incr (h, correct_code);
                if (last_code > 0)
                {
                    /* Add the current word on the last word's bigram */
                    bigram_incr (h->u[last_code].bi, correct_code);
                }
            }
            last_code = correct_code;
        }
        /* Reset values here */
        if (b_reset)
        {
            pt_reset_root (pt);
            b_word_recognized = FALSE;
            b_reset = FALSE;
            correct_code = UNDEFINED_WORD;
            read_code = UNDEFINED_WORD;
            i = 0;
        }
    }
    free (word);
}

/* Read the corpus word by word */
unsigned int parser_get_sequence_size (prefix_tree *pt, lattice *lat,
                                       int lang, debug *d)
{
    bool b_word_recognized = FALSE;
    bool b_reset = FALSE;

    int correct_code = UNDEFINED_WORD;
    int read_code = UNDEFINED_WORD;

    unsigned int i = 0, nb_code = 0, nb_undefined_code = 0;
    char *word = malloc (sizeof (char) * MAX_WORD_LENGTH);

    while ((word[i] = fgetc (lat->lat_file->file)) != EOF)
    {
        word[i+1] = '\0';

        /* Set special word and code for the new line */
        if ((word[0] == '\n') || (word[0] == ' '))
        {
            b_reset = TRUE;
        }
        /* If the single char is a hard separator */
        else if (is_hard_sep_general (word[0]) ||
                 (lang == EN && is_hard_sep_en (word[0])))
        {
            correct_code = pt_search (pt, word[i]);
            if (correct_code == UNDEFINED_WORD)
            {
                nb_undefined_code++;
            }
            else
            {
                b_word_recognized = TRUE;
            }
            b_reset = TRUE;
        }
        /* If the next char make the word undefined */
        else if ((read_code = pt_search (pt, word[i]))
                                                      == UNDEFINED_WORD)
        {
            /* The liaison char can be written differently */
            if (word[i] == '-')
            {
                if ((read_code = pt_search (pt, '_')) != UNDEFINED_WORD)
                {
                    correct_code = read_code;
                    i++;
                }
                else
                {
                    nb_undefined_code++;
                }
            }
            /* If the char is a hard separator, we remove the last
             * letter on the buffer and we keep the last correct_code */
            else if ((is_hard_sep_general (word[i])) ||
                        (lang == EN && is_hard_sep_en (word[i])) ||
                        (word[i] == '\''))
            {
                if (correct_code > 0)
                {
                    ungetc (word[i], lat->lat_file->file);
                    word[i--] = '\0';

                    b_word_recognized = TRUE;
                }
                b_reset = TRUE;
            }
            else
            {
                correct_code = read_code;
                i++;
            }
        }
        /* Stop the research if the last read char is an apostrophe */
        else if (word[i] == '\'' && i != 0)
        {
            correct_code = read_code;
            b_word_recognized = TRUE;
            b_reset = TRUE;
        }
        /* Everything is fine, we continue to read the word */
        else
        {
            correct_code = read_code;
            i++;
        }

        if (b_word_recognized)
        {
            /* Add the word on the lattice */
            nb_code++;
        }
        /* Reset values here */
        if (b_reset)
        {
            pt_reset_root (pt);
            b_word_recognized = FALSE;
            b_reset = FALSE;
            i = 0;
            correct_code = UNDEFINED_WORD;
            read_code = UNDEFINED_WORD;
        }
    }
    free (word);

    rewind (lat->lat_file->file);

    if (d->b_translation)
    {
        printf ("UNDEFINED WORD: %d\n", nb_undefined_code);
    }

    return nb_code;
}

/* Read the corpus word by word */
void parser_read_lattice (prefix_tree *pt, lattice *lat, hashtrans *tt,
                          perplexity *pplex, debug *d, int lang)
{
    bool b_word_recognized = FALSE;
    bool b_reset = FALSE;
    bool new_line = FALSE;

    int correct_code = UNDEFINED_WORD;
    int read_code = UNDEFINED_WORD;

    unsigned int i = 0, nb_word = 0;
    char *word = malloc (sizeof (char) * MAX_WORD_LENGTH);

    int lattice_size = parser_get_sequence_size (pt, lat, lang, d);

    lattice_init (lat, lattice_size, d);
    pplex->seq = malloc (sizeof (unsigned int) * lattice_size);
    pplex->size = lattice_size;

    while ((word[i] = fgetc (lat->lat_file->file)) != EOF)
    {
        word[i+1] = '\0';

        /* Set special word and code for the new line */
        if (word[0] == '\n')
        {
            b_reset = TRUE;
            new_line = TRUE;
        }
        if (word[0] == ' ')
        {
            b_reset = TRUE;
        }
        /* If the single char is a hard separator */
        else if (is_hard_sep_general (word[0]) ||
                 (lang == EN && is_hard_sep_en (word[0])))
        {
            correct_code = pt_search (pt, word[i]);
            b_word_recognized = (correct_code > 0);
            b_reset = TRUE;
        }
        /* If the next char make the word undefined */
        else if ((read_code = pt_search (pt, word[i]))
                                                      == UNDEFINED_WORD)
        {
            /* The liaison char can be written differently */
            if (word[i] == '-')
            {
                if ((read_code = pt_search (pt, '_')) != UNDEFINED_WORD)
                {
                    correct_code = read_code;
                    i++;
                }
            }
            /* If the char is a hard separator, we remove the last
             * letter on the buffer and we keep the last correct_code */
            else if (((is_hard_sep_general (word[i])) ||
                            (lang == EN && is_hard_sep_en (word[i])))
                        || (word[i] == '\''))
            {
                if (correct_code > 0)
                {
                    ungetc (word[i], lat->lat_file->file);
                    word[i--] = '\0';

                    b_word_recognized = TRUE;
                }
                b_reset = TRUE;
            }
            /* Or we update the correct_code with the last read_code */
            else
            {
                correct_code = read_code;
                i++;
            }
        }
        /* Stop the research if the last read char is an apostrophe */
        else if (word[i] == '\'' && i != 0)
        {
            correct_code = read_code;
            b_word_recognized = TRUE;
            b_reset = TRUE;
        }
        /* Everything is fine, we continue to read the word */
        else
        {
            correct_code = read_code;
            i++;
        }

        if (b_word_recognized)
        {
            /* Add the word on the lattice */
            if (correct_code > 0)
            {
                /* Print if debug */
                if (d->b_translation)
                {
                    printf ("Add to lattice: %s " "%d\n",
                                             word, correct_code);
                }
                lattice_add_word (lat, tt, nb_word, correct_code,
                                  new_line);
                pplex->seq[nb_word] = correct_code;
                nb_word++;
                new_line = FALSE;
            }
        }
        /* Reset values here */
        if (b_reset)
        {
            pt_reset_root (pt);
            b_word_recognized = FALSE;
            b_reset = FALSE;
            correct_code = UNDEFINED_WORD;
            read_code = UNDEFINED_WORD;
            i = 0;
        }
    }

    free (word);
}

void parser_read_perplex (perplexity *p, debug *d)
{
    unsigned int code;

    if (fscanf (p->pplx_file->file, "%u", &p->size) == EOF)
    {
        perror ("First val of the perplexity file must be the size.");
    }

    p->seq = malloc (sizeof (unsigned int) * p->size);

    while (fscanf (p->pplx_file->file, "%u", &code) != EOF)
    {
        if (d->b_perplexity)
        {
            printf ("Word number %d: %u\n", p->size, code);
        }
        p->seq[p->size++] = code;
    }

    if (d->b_perplexity)
    {
        perplexity_print_sequence (p);
    }
}

/* Do 3 time a loop to parse the translation table. */
void parser_read_translation_table (hashtrans *tt, debug *d)
{
    unsigned int *t_i;
    unsigned int c_fr, c_en;
    float logprob;
    unsigned int size = 0;

    /* Find the max french code value of the translation table */
    while (fscanf (tt->tt_file->file, "%u "  "%u "  "%f",
                                      &c_fr, &c_en, &logprob) != EOF)
    {
        if (c_fr >= size)
        {
            size = c_fr + 1;
        }
    }
    if (d->b_translation)
    {
        printf ("Translation table size: %u\n", size);
    }

    hashtrans_init (tt, size);

    /* Seak every word's equivalence size through a second loop on the
     * file */
    rewind (tt->tt_file->file);
    while (fscanf (tt->tt_file->file, "%u %u %f",
                                         &c_fr, &c_en, &logprob) != EOF)
    {
        tt->t_equiv[c_fr]->size++;
    }

    hashtrans_init_equiv_tables (tt);

    /* Add every translations with a third loop */
    rewind (tt->tt_file->file);
    t_i = calloc (sizeof (unsigned int), size);

    while (fscanf (tt->tt_file->file, "%u %u %f",
                                         &c_fr, &c_en, &logprob) != EOF)
    {
        translation_table_init (tt->t_equiv[c_fr]->t_trans[t_i[c_fr]],
                                                         c_en, logprob);
        if (d->b_translation)
        {
            printf ("Translation from %u to %u with proba %f\n",
                                                   c_fr, c_en, logprob);
        }
        t_i[c_fr]++;
    }

    free (t_i);
}

void parser_write_sequence (unsigned int *seq, lattice *lat,
                            language *en, debug *d)
{
    unsigned int i;

    if (d->b_translation)
    {
        printf ("Translated text:\n");
    }

    for (i = 0; i < lat->size; i++)
    {
        if (lat->t_word[i]->new_line)
        {
            printf ("\n");
        }
        printf ("%s", en->pt->t_word[seq[i]]);

        if (i != lat->size - 1)
        {
            printf (" ");
        }
    }
    printf ("\n");
}
