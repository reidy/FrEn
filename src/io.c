/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "io.h"

io *io_alloc (unsigned int mode)
{
    io *f = malloc (sizeof (io));
    f->custom = FALSE;
    f->mode = mode;
    f->file = NULL;

    return f;
}

void io_open_failure (io *f)
{
    char *err_msg = malloc (sizeof (char) * 100);

    sprintf (err_msg, "I/O file %.50s can't be opened.", f->location);
    perror (err_msg);

    free (err_msg);
    exit (EXIT_FAILURE);
}

void io_open (io *f)
{
    switch (f->mode)
    {
        case READ:
            f->file = fopen (f->location, "r");
            break;
        case WRITE:
            f->file = fopen (f->location, "w");
            break;
        case APPEND:
            f->file = fopen (f->location, "a");
            break;
        case READ_U:
            f->file = fopen (f->location, "r+");
            break;
        case WRITE_U:
            f->file = fopen (f->location, "w+");
            break;
        case APPEND_U:
            f->file = fopen (f->location, "a+");
            break;
        default:
            printf ("Bad opening mode.\n");
            exit (EXIT_FAILURE);
    }

    if (f->file == NULL)
    {
        io_open_failure (f);
    }
}

void io_close (io *f)
{
    if (f->file != NULL)
    {
        fclose (f->file);
    }
}

void io_free (io *f)
{
    io_close (f);
    free (f);
}
