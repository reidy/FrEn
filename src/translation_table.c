/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "translation_table.h"

translation_table **translation_table_alloc (unsigned int size)
{
    unsigned int i;

    translation_table **t = malloc (sizeof (translation_table*) * size);

    for (i = 0; i < size; i++)
    {
        t[i] = malloc (sizeof (translation_table));
    }

    return t;
}

void translation_table_init (translation_table *t, unsigned int c_en,
                             float logprob)
{
    t->logprob = logprob;
    t->c_en = c_en;
}

void translation_table_free (translation_table *t)
{
    free (t);
}
