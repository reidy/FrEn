/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "perplexity.h"

perplexity *perplexity_alloc (void)
{
	perplexity *p = malloc (sizeof (perplexity));
	p->seq = NULL;
    p->pplx_file = io_alloc (READ);
	p->size = 0;
	p->pplex = 0.0;
    p->custom = FALSE;
	
	return p;
}

/* Init and launch the perplexity computation */
void perplexity_seq_size (perplexity *p)
{
    printf ("What is the length of the sequence? ");

    do
    {
        scanf ("%5u", &p->size);
        getc (stdin);

        if (p->size <= 0)
        {
            perror ("Length must be a positive value.");
        }
    } while (p->size <= 0);
}

void perplexity_seq_fill (perplexity *p)
{
    unsigned int i;

    printf ("Fill the sequence with word code separated by space:\n");
    for (i = 0; i < p->size; i++)
    {
        fscanf (stdin, "%8u", &p->seq[i]);
        getc (stdin);
    }
}

void perplexity_launch (hashgram *h, perplexity *p, debug *d)
{
    bool retry = FALSE;

    do
    {
        if (retry)
        {
            free (p->seq);
            printf ("Insert a new sequence and retry.\n");
            p->custom = TRUE;
        }
        if (p->custom)
        {
            perplexity_seq_size (p);

            p->seq = malloc (sizeof (unsigned int) * p->size);
            perplexity_seq_fill (p);
        }
        else
        {
            parser_read_perplex (p, d);
        }
        retry = TRUE;
    } while (perplexity_computation (h, p, d) == FALSE);
}

/* Perplexity calculus */
bool perplexity_verify_value (unsigned int max_val, perplexity *p,
                              debug *d)
{
    unsigned int i;

    for (i = 0; i < p->size; i++)
    {
        if (d->b_perplexity)
        {
            printf ("Max size: %d, val: %d\n", max_val, p->seq[i]);
        }
        if (p->seq[i] > max_val)
        {
            return FALSE;
        }
    }
    return TRUE;
}

float perplexity_comp_alpha (hashgram *h, perplexity *p, unsigned int i,
                             debug *d)
{
    float perplex = 0;
    bigram *tmp = h->u[p->seq[i - 1]].bi;

    while (tmp != NULL)
    {
        if (tmp->id == (int)p->seq[i])
        {
            printf ("%f\n", tmp->p);
            perplex += tmp->p;
            break;
        }

        tmp = tmp->next;
    }

    perplex += h->u[p->seq[i - 1]].p + 5;

    if (d->b_perplexity)
    {
        printf ("P * (%d"           "|%d) = "            "%f\n",
                      p->seq[i], p->seq[i - 1], perplex);
    }

    return perplex;
}

bool perplexity_computation (hashgram *h, perplexity *p, debug *d)
{
    unsigned int i;

    if (perplexity_verify_value (h->size, p, d) == FALSE)
    {
        return FALSE;
    }

    /* Run the sequence */
    for (i = 1; i < p->size; i++)
    {
        if (p->seq[i] > h->size)
        {
            printf ("Number not present on the hashgram table.\n");
        }

        p->pplex += perplexity_comp_alpha (h, p, i, d);
    }

    /* Perplexity of the first elem of the sequence */
    p->pplex += h->u[p->seq[0]].p;

    p->pplex = p->pplex / p->size;

    if (isnan (p->pplex))
    {
        perror ("Perplexity is not a number");
        return FALSE;
    }

    printf ("Perplexity : %f\n", p->pplex);
    return TRUE;
}

void perplexity_print_sequence (perplexity *p)
{
    unsigned int i;

    printf ("Perplexity sequence: ");

    for (i = 0; i < p->size; i++)
    {
        printf ("%u ", p->seq[i]);
    }

    printf ("\n");
}

void perplexity_free (perplexity *p)
{
    if (p->custom || p->seq != NULL)
	{
		free(p->seq);
	}

    io_free (p->pplx_file);
	free (p);
}
