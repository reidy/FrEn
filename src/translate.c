/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "translate.h"

translate *translate_alloc (void)
{
    translate *t = malloc (sizeof (translate));
    t->table = hashtrans_alloc ();
    t->lat = lattice_alloc ();
    t->pplex = perplexity_alloc ();
    t->pplex->custom = TRUE;

    return t;
}

void translate_process (language *en, language *fr, translate *t,
                        debug *d)
{
    unsigned int *seq;

    hashtrans_init_io (t->table, d);
    parser_read_translation_table (t->table, d);
    translate_create_lattice (fr, t, d);

    if (d->b_perplex_analysis)
    {
        perplexity_print_sequence (t->pplex);
        perplexity_computation (fr->hash, t->pplex, d);
    }

    if (t->lat->size == 0)
    {
        perror ("Couldn't parse a sequence, check your file.");
        return;
    }

    seq = viterbi (t->lat, t->table, en->hash, d);

    if (d->b_translation)
    {
        lattice_print (t->lat);
    }

    /*seq = viterbi_transalte_sequence (t->lat);*/
    parser_write_sequence (seq, t->lat, en, d);

    free (seq);
}

void translate_create_lattice (language *fr, translate *t, debug *d)
{
    lattice_init_io (t->lat);
    parser_read_lattice (fr->pt, t->lat, t->table, t->pplex, d, FR);
}

void translate_free (translate *t)
{
    hashtrans_free (t->table);

    if (t->lat != NULL)
    {
        lattice_free (t->lat);
    }

    perplexity_free (t->pplex);

    free (t);
}
