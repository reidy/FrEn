/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hashgram.h"

/* Alloc the hashgram */
hashgram *hashgram_alloc (void)
{
    hashgram * h = malloc (sizeof (hashgram));
    h->u = NULL;
    h->size = 0;

    return h;
}

/* Count the total number of word */
void hashgram_n_words (hashgram *h)
{
    unsigned int i;

    for (i = 0; i < h->size; i++)
    {
        h->n_words = h->n_words + h->u[i].c;
    }
}

/* Init the hashgram */
void hashgram_init (hashgram *h, unsigned int size)
{
    h->size = size + 1;
    h->n_words = 0;
    unigram_alloc (h);
    unigram_init (h);
}

/* Print the hashgram */
void hashgram_print (hashgram *h, debug *d)
{
    if (d->b_hashgram)
    {
        unigram_print (h);
    }
}

/* Free the hashgram */
void hashgram_free (hashgram *h)
{
    unsigned int i;

    for (i = 0; i < h->size; i++)
    {
        unigram_free (&h->u[i]);
    }
    if (h->u != NULL)
    {
        free (h->u);
    }

    free (h);
}
