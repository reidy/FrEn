/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "conf.h"

/* Free all */
void main_free_all (language *en, language *fr, translate *t, debug *d)
{
    language_free (fr);
    language_free (en);
    translate_free (t);
    debug_free (d);

    exit (EXIT_SUCCESS);
}

int main (int argc, char **argv)
{
    language *en = language_alloc ();
    language *fr = language_alloc ();
    debug *d = debug_alloc ();
    translate *t = translate_alloc ();

    language_init (en, EN);
    language_init (fr, FR);

    /* Check launching arguments */
    if (cmdline_read_arg (argc, argv, en, fr, t, d))
    {
        main_free_all (en, fr, t, d);
    }

    /* Init lexicon, corpus and other structures */
    language_init_data (en, d);
    language_init_data (fr, d);

    /* Translation process */
    translate_process (en, fr, t, d);

    /* Free memories and leave*/
    main_free_all (en, fr, t, d);

    return 0;
}
