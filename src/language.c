/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "language.h"

language *language_alloc (void)
{
    language *l = malloc (sizeof (language));

    return l;
}

void language_init (language *l, unsigned int type)
{
    l->hash = hashgram_alloc ();
    l->pt = pt_alloc ();
    l->lexi = io_alloc (READ);
    l->corp = io_alloc (READ);
    l->perplex = perplexity_alloc ();

    l->type = type;
}

void language_init_data (language *l, debug *d)
{
    language_init_io (l);
    language_parse (l, d);
    language_perplex (l, d);
}

void language_init_io (language *l)
{
    if (!l->lexi->custom)
    {
        switch (l->type)
        {
            case FR:
                l->lexi->location = DEFAULT_FR_LEXICON;
                break;
            case EN:
                l->lexi->location = DEFAULT_EN_LEXICON;
                break;
            default:
                perror ("Unknown language.");
                exit (EXIT_FAILURE);
        }
    }
    io_open (l->lexi);

    if (!l->corp->custom)
    {
        switch (l->type)
        {
            case FR:
                l->corp->location = DEFAULT_FR_CORPUS;
                break;
            case EN:
                l->corp->location = DEFAULT_EN_CORPUS;
                break;
            default:
                perror ("Unknown language.");
                exit (EXIT_FAILURE);
        }
    }
    io_open (l->corp);

    if (!l->perplex->custom)
    {
        switch (l->type)
        {
            case FR:
                l->perplex->pplx_file->location = DEFAULT_FR_PERPLEXITY;
                break;
            case EN:
                l->perplex->pplx_file->location = DEFAULT_EN_PERPLEXITY;
                break;
            default:
                perror ("Unknown language.");
                exit (EXIT_FAILURE);
        }
        io_open (l->perplex->pplx_file);
    }
}

void language_parse (language *l, debug *d)
{
    parser_read_lexicon (l->pt, l->lexi, d);
    hashgram_init (l->hash, l->pt->size);
    parser_read_corpus (l->pt, l->corp, l->hash, d, l->type);
}

void language_perplex (language *l, debug *d)
{
    char response = 'N', retry = 'N';

    probability_launch (l->hash);
    hashgram_print (l->hash, d);

    if (d->b_perplex_analysis != TRUE)
    {
        return;
    }

    printf ("Do you want to test a perplexity sequence on %s? [Y/N]\n",
            (l->type == EN) ? "EN" : "FR" );

    do
    {
        response = getc (stdin);
        getc (stdin);

        if (response == 'Y')
        {
            do
            {
                perplexity_launch (l->hash, l->perplex, d);
                printf ("Do you want to test another sequence? "
                        "[Y/N]\n");

                retry = getc (stdin);
            } while (retry == 'Y');
        }
    } while ((response != 'Y' && response != 'N') || retry == 'Y');
}

void language_free (language *l)
{
    io_free (l->lexi);
    io_free (l->corp);
    perplexity_free (l->perplex);
    hashgram_free (l->hash);
    pt_free (l->pt);
    free (l);
}
