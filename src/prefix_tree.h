/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PREFIX_TREE__H
#define PREFIX_TREE__H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define UNDEFINED_WORD -1
#define NEW_LINE 0

typedef struct 
{
    char letter;
    int code;
} node;

typedef struct tree tree;
struct tree
{
    node *n;
    tree *left_son;
    tree *right_son;
};

typedef struct
{
    tree *root;
    tree *current;
    unsigned int size;
    char **t_word;
} prefix_tree;

prefix_tree *pt_alloc ();
void pt_add_word (prefix_tree *pt, char *word, unsigned int code);
void pt_search_and_go (prefix_tree *pt, char letter);
int pt_search (prefix_tree *pt, char letter);
void pt_reset_root (prefix_tree *pt);
void pt_print (tree *t);
void pt_print_tab_word (prefix_tree *pt);
void pt_empty (tree *t);
void pt_free (prefix_tree *pt);

#endif
