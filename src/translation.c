/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "translation.h"

translation **translation_alloc (unsigned int size)
{
    translation **t = malloc (sizeof (translation*) * size);

    return t;
}

void translation_init (translation *t, unsigned int id,
                       float alpha, unsigned int beta)
{
    t->id = id;
    t->alpha = alpha;
    t->beta = beta;
}

void translation_free (translation *t)
{
    free (t);
}
