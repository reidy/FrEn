/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IO__H
#define IO__H

#include <sys/file.h> 
#include "conf.h"

#define READ 0
#define WRITE 1
#define APPEND 2
#define WRITE_U 3
#define READ_U 4
#define APPEND_U 5

io *io_alloc (unsigned int mode);
void io_open (io *f);
void io_close (io *f);
void io_free (io *f);

#endif
