/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HASHGRAM__H
#define HASHGRAM__H

#include "conf.h"

hashgram *hashgram_alloc (void);
void hashgram_n_words (hashgram *h);
void hashgram_init (hashgram *h, unsigned int size);
void hashgram_print (hashgram *h, debug *d);
void hashgram_free (hashgram *h);

#endif
