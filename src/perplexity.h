/*
 *  Copyright (C) 2015  Bacterium & Reid
 *
 *  This file is part of FrEn.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PERPLEXITY__H
#define PERPLEXITY__H

#include "conf.h"

#define LAMBDA_1 0.95
#define LAMBDA_2 0.05

perplexity *perplexity_alloc (void);
void perplexity_launch (hashgram *h, perplexity *p, debug *d);
bool perplexity_computation (hashgram *h, perplexity *p, debug *d);
void perplexity_print_sequence (perplexity *p);
void perplexity_free (perplexity *p);

#endif
