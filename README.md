# FrEn

Natural Language Processing translator program using a machine learning 
process with an implementation of the [Viterbi algorithm] to calculate the 
best probability of a translation.


## Library
 - ISO C library


## Introduction
This program uses three general kind of parsers:
- The first parses dictionnaries of the input and output language inside a 
binary search tree.
- The second parses a corpus to feed the "learning" part of the program 
into a so called "HashGram" database with bi-grams data containing a 
couple (sequence of two words).
- The third and last parses a translation table with input words, output 
words, and the matching probability to have that translation into a
"HashTrans" structure.

Probabilities from the HashTrans and the HashGram will be used to 
generate a most likely sequence translation through the execution of the 
[Viterbi algorithm].


## Options
-  -D, --debug             : debug all.
-  -L, --debug-lex         : debug lexicons.
-  -C, --debug-corp        : debug corpuses.
-  -T, --debug-tree        : debug prefix trees.
-  -H, --debug-hashgram    : debug hashgrams.
-  -P, --debug-perplexity  : debug perplexities.
-  -A, --debug-translation : debug translation.
-  -V, --debug-viterbi     : debug viterbi algorithm.
-  -k, --en-lexicon \[arg\]  : English lexicon text file path.
-  -l, --fr-lexicon \[arg\]  : French lexicon text file path.
-  -b, --en-corpus \[arg\]   : English corpus text file path.
-  -c, --fr-corpus \[arg\]   : French corpus text file path.
-  -o, --en-perplex \[arg\]  : English perplexity text file path.
-  -p, --fr-perplex \[arg\]  : French perplexity text file path.
-  -a, --translation \[arg\] : translation table file path.
-  -z, --lattice \[arg\]     : file path to translate.
-  -Y, --perplexity        : enable the perplexity analysis.
-  -h, --help              : print this help.

## Code style
C ANSI.
AddressSanitizer (ASan) and pedantic options enabled.
Code is intended with 4 spaces, no tabs.  
A line should not exceed 72 characters.  
It follows Allman indent style.

[Viterbi algorithm]:http://www.cambridge.org/resources/0521882672/7934_kaeslin_dynpro_new.pdf
